'use strict';

import Helpers from './helpers.js';
import Modal from './modal.js';
import CarritoDeCompras from './carrito-compras.js';
import Producto from './productos.js';


document.addEventListener('DOMContentLoaded', async event => {

    Helpers.cargarPagina(
        '#index-header' , 
        './resources/views/menu.html'
    ).then(
        gestionarOpciones
    ).catch(error => 
        Helpers.alertar('#index-contenido' , 'Problemas al acceder al menú principal', error)
    );
    
    await Modal.crear();
    //Modal.desplegar({
      //  titulo: 'Bienvenido',
        //contenido: 'Ahora puede hacer sus compras aqui.'
    //});
});


let gestionarOpciones = resultado => {
    let elemento = `#${resultado.id}`; // se asigna '#index-header'

    cargarProductos(elemento);
    cargarCarrito(elemento);
    cargarContactenos(elemento);
    cargarNosotros(elemento);
    cargarActualizarDatos(elemento);
    cargarCambiarPassword(elemento);


}

let cargarProductos = elemento => {
    let referencia = document.querySelector(`${elemento} a[id='menu-productos']`);

    referencia.addEventListener('click', async event => {
        event.preventDefault();
        let producto = await Producto.crear();
        producto.gestionarProductos();
    });
}

let cargarCarrito = elemento => {
    let referencia = document.querySelector(`${elemento} a[id='menu-carrito-compras']`);
    referencia.addEventListener('click', async (event) => {
        event.preventDefault();
        let carrito = await CarritoDeCompras.crear();
        carrito.gestionarVentas();
    });
    return referencia;
}

let cargarContactenos = elemento => {
    let referencia = document.querySelector(`${elemento} a[id='menu-contactenos']`);
    referencia.addEventListener('click', (event) => {
        event.preventDefault();
        Helpers.cargarPagina('#index-contenido', './resources/views/contactenos.html')
        .catch(error => {
            Helpers.alertar('#index-contenido', 'Problema al acceder a contactenos', error);
        });
    });
    return referencia;
}

let cargarActualizarDatos = elemento => {
    let referencia = document.querySelector(`${elemento} a[id='menu-actualizar-datos']`);
    referencia.addEventListener('click', (event) => {
        event.preventDefault();
        Helpers.cargarPagina('#index-contenido', './resources/views/actualizar-datos.html')
        .catch(error => {
            Helpers.alertar('#index-contenido', 'Problema al acceder a actualizar datos', error);
        });
    });
    return referencia;
}

let cargarCambiarPassword = elemento => {
    let referencia = document.querySelector(`${elemento} a[id='menu-cambio-contraseña']`);
    referencia.addEventListener('click', (event) => {
        event.preventDefault();
        Helpers.cargarPagina('#index-contenido', './resources/views/cambio-contraseña.html')
        .catch(error => {
            Helpers.alertar('#index-contenido', 'Problema al acceder a cambio contraseña', error);
        });
    });
    return referencia;
}

let cargarNosotros = elemento => {
    let referencia = document.querySelector(`${elemento} a[id='menu-nosotros']`);
    referencia.addEventListener('click', (event) => {
        event.preventDefault();
        Helpers.cargarPagina('#index-contenido', './resources/views/nosotros.html')
        .catch(error => {
            Helpers.alertar('#index-contenido', 'Problema al acceder a nosotros', error);
        });
    });
    return referencia;
}   
;




    