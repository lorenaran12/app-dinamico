export default class Helpers {

    /**
     * Carga en elemento el recurso html obtenido de una URL
     * @param {String} elemento el ID de un elemento HTML (se tiene en cuenta la sintaxis de los selectores CSS )
     * @param {String} url la dirección local o remota del recurso del que se obtiene el código html.
     */
    static async cargarPagina(elemento, url) {
        // se intenta encontrar y obtener el recurso dado por la URL
        let respuesta = await fetch(url);

        if (respuesta.ok) {
            // Si se pudo obtener el recurso se referencia el contenedor
            const contenedor = document.querySelector(elemento);
            // Se asigna al contenedor el código HTML obtenido del recurso
            contenedor.innerHTML = await respuesta.text();
            return contenedor;
        }

        throw `error ${respuesta.status} - ${respuesta.statusText}`;
    }

    /**
     * Intenta obtener en formato JSON, un recurso indicado por la URL del primer argumento
     */
    static leerJSON = async (url, opciones = {}) => {
        let respuesta = await fetch(url, opciones);
        if (respuesta.ok) {
            return await respuesta.json();
        }
        throw new Error(`${respuesta.status} - ${respuesta.statusText}`);
    }

    function () {
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
            interval: 2000
        });
    
    
    
    };
    

    // proponer que mejor se ingrese un objeto con: elemento, mensaje, error y atencion
    // el contenido de error iría x consola y se podría desactivar o mostrar
    // con un getFlag proporcionado por esta clase
    static alertar(elemento, mensaje, error = '', atencion = '¡Santo cielo!') {
        // ver https://tailwindcss.com/components/alerts#left-accent-border
        document.querySelector(elemento).insertAdjacentHTML('afterbegin', `
            <div id="alerta" class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert">
                <p class="font-bold">${atencion}</p>
                <p>${mensaje}</p>
            </div>
        `);

        setTimeout(() => document.querySelector('#alerta').style.display = 'none', 3000);

        if (error) {
            console.error(`Houston, tenemos un problema: ${error}`);
        }
    }

    /**
     * Devuelve un entero aleatorio entre min (inclusive) y max (inclusive).
     * El valor no es menor que min (es el siguiente entero mayor que min si min no es un entero) 
     * y no es mayor que max (el siguiente entero menor que max si max no es un entero).
    */
    static getRandomInt = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static existeElemento(idElemento) {
        let elemento = document.querySelector(idElemento);
        return (typeof(elemento) != 'undefined' && elemento != null);
    }

}
