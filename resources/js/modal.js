import Helpers from './helpers.js';

export default class Modal {

    static async crear() {
        await Helpers.cargarPagina(
            '#index-modal',
            './resources/views/modal.html'
        ).catch(error =>
            console.error(error)
        );
    }

    static desplegar({ titulo, contenido, botones = [] }) {
        document.querySelector('.modal-content #edicionModalLabel').innerHTML = titulo;
        document.querySelector('.modal-content main').innerHTML = contenido;
        let footer = document.querySelector('.modal-content #modal-footer');
        footer.innerHTML = '';

        botones.forEach((objBoton) => {
            let htmlBoton = `
                <button id="${objBoton.id}" class="${objBoton.clase}">
                ${objBoton.titulo}</button>`;

            footer.insertAdjacentHTML('beforeend', htmlBoton);
            let boton = document.querySelector(`.modal-content #${objBoton.id}`);
            if (typeof objBoton.callBack === 'function') {
                boton.addEventListener('click', e => objBoton.callBack(e));
            }
        });
        Modal.asignarEventos();
        Modal.toggle();
    }

    static asignarEventos() {
        const overlay = document.querySelector('.modal-overlay');
        overlay.addEventListener('click', Modal.toggle);

        let closeModals = document.querySelectorAll('.modal-close');
        closeModals.forEach(closeModal => closeModal.addEventListener('click', Modal.toggle));

        document.onkeydown = evt => {
            let isEscape = false;

            if ("key" in evt) {
                isEscape = (evt.key === "Escape" || evt.key === "Esc")
            } else {
                isEscape = (evt.keyCode === 27)
            }

            if (isEscape && document.body.classList.contains('modal-active')) {
                Modal.toggle();
            }
        };
    }

    static toggle() {
        const body = document.querySelector('body');
        const modal = document.querySelector('.modal');

        modal.classList.toggle('opacity-0');
        modal.classList.toggle('pointer-events-none');
        body.classList.toggle('modal-active');
    }
}