import Helpers from './helpers.js';

export default class CarritoDeCompras {

    #porComprar;
    #descuento;
    #productos;

    constructor() {
        this.#productos = [];//Array con el stock de productos
        this.#porComprar= [
            //{ indice: 0, cantidad:1},
        ]; 
        this.#descuento = 12;
    }
    
    
    static async crear() {
        const instancia = new CarritoDeCompras();

        await Helpers.cargarPagina(
            '#index-contenido', './resources/views/carrito-compras.html'
        ).catch(error =>
            Helpers.alertar(
                '#index-contenido',
                'Problemas al acceder al carrito de compras', error
            )
        );
        console.log('Cargada la página del carrito');

        instancia.#productos = await Helpers.leerJSON(
            './data/productos.json').catch(error =>
                Helpers.alertar(
                    '#index-contenido',
                    'Problemas al acceder a los productos', error
                )
            );
        console.log('Cargados los productos', instancia.#productos);

        return instancia;
    }

    gestionarVentas(){
        this.#productos.forEach((producto, indice) =>{
            let idEnlace= `carrito-producto-${indice}`;
            let sliderItem = ``;
            let fichaProducto = `
            
                <div class="card w-full flex flex-col p-3 overflow-hidden flex-1">
                    <img 
                    src="./resources/assets/images/${producto.imagen}" 
                    alt="..." 
                    class="card-img-top w-full h-64" />
                    <div class="p-4 flex-1 flex flex-col card-body">
                        <h3 class="card-title mb-4 text-2xl">${producto.referencia} - ${producto.precio}</h3>
                        <div class="mb-4 text-sm flex-1">
                            <p>${producto.resumen}</p>
                        </div>
                    </div>
                        <a id="${idEnlace}" data-indice="${indice}" href="#!"
                            class="btn btn-primary bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded
                                    focus:outline-none focus:shadow-outline text-center"
                            >AGREGAR AL CARRITO
                        </a>
                    </div>
                </div>
            
        `;
        document.querySelector('#carrito-disponibles').insertAdjacentHTML('beforeend', fichaProducto);
        document.querySelector(`#${idEnlace}`).addEventListener('click', e => {
            this.agregaAlCarrito(e.target.dataset.indice);

        });
        });
        let btnPagar = document.querySelector('#carrito-btnpagar');
        btnPagar.style.display = 'none'; //Visible si hay elementos en el carrito
        btnPagar.addEventListener('click', event => this.procesarPago());
        let btnvaciar = document.querySelector('#carrito-btnvaciar');
        btnPagar.style.display= 'none';
        btnvaciar.addEventListener('click', event => this.vaciarCarrito());
        let mensajeCupon = document.querySelector('#mensaje-cupon');
        mensajeCupon.insertAdjacentHTML('beforeend', this.mensajeRandom());
    }

    eliminarDelCarrito(indice) {
        // eliminar la ficha de la lista de compras
        let elemento = document.querySelector(`#carrito-venta-${indice}`);
        elemento.parentNode.removeChild(elemento); // distinto a dejarlo vacío

        // eliminar el elemento del array de los productos a comprar 
        let item = this.#porComprar.find(
           producto => producto.indice === indice
        );
        let i = this.#porComprar.indexOf(item);
        this.#porComprar.splice(i, 1);


        // si no quedan elementos por comprar ocultar el botón de pago
        if (this.#porComprar.length === 0) {
            document.querySelector('#carrito-btnpagar').style.display = 'none';
        }
    }

    confirmarPago() {

        if (Helpers.existeElemento('#carrito-orden-envio')) {
            return;
        }

        this.#porComprar =[];
        document.querySelector('#carrito-btnpagar').style.display = 'none';
        document.querySelector('#carrito-btnconfirmar').style.display= 'none';
        document.querySelector('#carrito-elegidos').innerHTML = '';

        
        let nroOrden = Helpers.getRandomInt(10000, 9999999);

        let confirmacion = `
            <div id="carrito-orden-envio"
                class="card bg-white rounded shadow px-10 py-6 w-full
                       flex flex-wrap justify-center ">


                <div class="pr-8">
                    <h4 class="text-2xl font-bold">
                    Gracias por su compra
                    </h3>

                    <h6 id="carrito-nro-orden" class="text-sm 
                    font-bold">
                        ORDEN DE ENVÍO #${nroOrden}</h4>
                </div>

                <img src="https://image.flaticon.com/icons/svg/1611/1611768.svg"
                     alt="" class="w-24"
                     id="img-2">
            </div>
        `;

        document.querySelector('#carrito-confirmacion')
           .insertAdjacentHTML('beforeend', confirmacion);
    }


    procesarPago() {

        let total = 0;
        this.#porComprar.forEach(producto => {
            const totalLinea = this.#productos[producto.indice].precio*producto.cantidad;
            //reducir del disponible la cantidad vendida
            this.#productos[producto.indice].disponible -= producto.cantidad;
            total += totalLinea;
        });
        //Aplicar deducciones e impuestos
        const iva = total * 0.19;
        const descuento = total * (this.descuento / 100);
        const totalPago = total - descuento + iva;
        
        let pago = `
            <div class="card w-full ">
                <div class="w-full">
                    <h3 class="font-bold">Resumen del pago</h3>
                    <div class="flex justify-between">
                       <div class="text-xl font-bold">Valor</div>
                       <div class="text-xl text-right font-bold">$${total}</div>
                    </div>
                    <div class="flex justify-between">
                        <div class="text-xl font-bold">
                             IVA (19%)
                        </div>
                        <div class='text-xl text-right font-bold'>$${iva}</div>
                    </div>
                    <div class="w-full"></div>
                    <div class="flex justify-between">
                        <div class="text-xl font-bold">
                             Total a pagar
                        </div>
                        <div class="text-2xl font-bold">
                             $${totalPago}
                        </div>
                    </div>
                    <button id="carrito-btnconfirmar"
                        type="button"
                        class="btn btn-success text-white w-full
                        rounded shadow font-bold">
                        CONFIRMAR
                    </button>
                </div>
            </div>
        `;
        document.querySelector('#carrito-confirmacion').innerHTML = pago;
        document.querySelector('#carrito-btnconfirmar')
           .addEventListener('click', event => this.confirmarPago()
        )
    }

    vaciarCarrito() {
        this.#porComprar = [];

        document.querySelector('#carrito-elegidos').innerHTML='';
        document.querySelector('#carrito-btnpagar').style.display = 'none';

    }

    mensajeRandom(){
       let n1= Helpers.getRandomInt(0, 10);
       let n2= Helpers.getRandomInt(0, 10);

       if (n1 !== n2){
        document.querySelector('#mensaje-cupon').innerHTML='';
        document.querySelector('#mensaje-cupon').style.display = 'none';
       }
      
    }



    agregaAlCarrito(indice) {

        let idBtnEliminar = `carrito-btneliminar-${indice}`;

        let idLista = `lstcantidad-${indice}`; //**************OJO
        
        
        let disponibles = this.#productos[indice].disponible;
        if(disponibles === 0){
            alert('Ya no hay stock disponible')
            return

        }
        
        let item = this.#porComprar.find(producto => producto.indice === indice);
        let precio = this.#productos[indice].precio;

        if (item) {
            document.querySelector(`#carrito-venta-${item.indice}`).scrollIntoView();
            document.querySelector(`#lstcantidad-${item.indice}`).focus();
            return;
        }

        document.querySelector('#carrito-confirmacion').innerHTML = ''; //*******OJO */

        this.#porComprar.push({
            indice,
            cantidad: 1,
            precio:precio
        });

        let elementosLista = '<option>1</option>';
        for (let i = 2; i <= disponibles; i++) {
            elementosLista += `<option>${i}</option>`;
        }

        let producto = `
                <div id="carrito-venta-${indice}"
                    class="card w-full rounded flex p-4 justify-between flex-wrap">
                    <div class="row">
                        <div class="col-md-7">
                            <h3 class="text-lg font-medium">${this.#productos[indice].referencia}</h3>
                            <h6 class="text-red-700 text-xs font-bold mt-1">Sólo quedan ${disponibles} en stock </h4>
                        </div>
                        <div class="col-md-5">
                            <h5 class="text-2xl font-medium">
                                <sup class="text-lg text-teal-600">$</sup>
                                ${this.#productos[indice].precio}
                            </h5>
                            <h5 class="text-sm font-bold text-teal-800">Descuento ${this.#descuento}%</h5>
                        </div>

                        </div>
                        <div class="w-full flex justify-between">

                            <button id="${idBtnEliminar}" data-indice="${indice}" 
                                    class="btn btn-outline-danger" type="button">ELIMINAR</button>
                            <label class="block uppercase"
                                for="grid-first-name">
                                UNIDADES
                            <select id="lstcantidad-${indice}" data-indice="${indice}"
                                class="browser-default custom-select text-sm text-white p-2 rounded leading-tight">
                                ${elementosLista}
                            </select>
                            </label>    
                        </div>
                    </div>
            
            `;

        document.querySelector('#carrito-elegidos').insertAdjacentHTML('beforeend', producto);
        document.querySelector('#carrito-btnpagar').style.display = '';

        document.querySelector(`#${idBtnEliminar}`).addEventListener('click', e => {
            this.eliminarDelCarrito(e.target.dataset.indice);
        });
        document.querySelector(`#${idLista}`).addEventListener('change', event => 
        this.actualizarCantidadCompra(event));
    }

    actualizarCantidadCompra(e){
        //ubicar la posición del elemento porComprar dado el indice del data
        let indice = e.target.dataset.indice;
        let item = this.#porComprar.find(producto => producto.indice === indice);
        //cambiarle la cantidad, segín el valor seleccionado de la lista
        item.cantidad = parseInt(e.target.value); 
        
    }



};
