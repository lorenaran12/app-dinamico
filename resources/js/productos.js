import Helpers from './helpers.js';
import Modal from './modal.js';

export default class Productos {
    #productos;
    #formEdicion;

    constructor() {
        this.#productos = []
    }

    static async crear() {
        const instancia = new Productos();

        await Helpers.cargarPagina(
            '#index-contenido', './resources/views/productos.html'
        ).then(() => 
            instancia.#formEdicion = document.querySelector('#bloque-edicion').innerHTML
        ).catch(error =>
            Helpers.alertar(
                '#index-contenido',
                'Problemas al acceder a la página de productos', error
            )
        );

        instancia.#productos = await Helpers.leerJSON(
            './data/productos.json').catch(error =>
                Helpers.alertar(
                    '#index-contenido',
                    'Problemas al acceder a los productos', error
                )
            );

        return instancia;
    }

    gestionarProductos() {
        let filas = '';

        this.#productos.forEach((producto, indice) => {
            filas += `
            <table class="table">
                <thead>
                    <tr>
                        <td class="border">${producto.id}</td>
                        <td class="border text-center">
                            <img class="img-fluid" src="/resources/assets/images/${producto.imagen}">
                            <button id="producto-btndetalles-${indice}" type="button" class="modal-open btn btn-outline-dark"
                            data-ripple-color="dark">${producto.referencia}</button>
                        </td>
                        <td class="border">${producto.resumen}</td>
                        <td class="border text-cente">${producto.disponible}</td>
                        <td class="border text-cente">${producto.precio}</td>
                        <td class="border text-center">
                            <button id="producto-btneditar-${indice}" class="button far fa-edit" title="Editar"></button>
                            <button id="producto-btneliminar-${indice}" class="button fas fa-trash-alt" title="Eliminar"></button>
                        </td>
                    </tr>
                </thead>
            </table>`
            ;
        });

        document.querySelector('#productos-tabla > #filas-tabla').innerHTML = filas;

        for (let i = 0; i < this.#productos.length; i++) {
            document.querySelector(`#producto-btndetalles-${i}`).addEventListener(
                'click', e => this.verDetalles(e, i)
            );
            document.querySelector(`#producto-btneditar-${i}`).addEventListener(
                'click', e => this.editarFila(e, i)
            );
            document.querySelector(`#producto-btneliminar-${i}`).addEventListener(
                'click', e => this.eliminarFila(e, i)
            );
        }

        document.querySelector('#producto-agregar').addEventListener(
            'click', e => this.agregarFila(e)
        );
    }


    agregarFila(e) {
        event.preventDefault();
        Modal.desplegar({
            titulo: 'Agregar producto',
            contenido: this.formularioAdicion(),
            botones: [
                {
                    id: 'btn-guardar-infoproducto',
                    clase: 'modal-close',
                    titulo: 'Guardar',
                    callBack: this.guardarAdicion
                },
                {
                    id: 'btn-cerrar-infoprooducto',
                    clase: 'modal-close',
                    titulo: 'Cerrar'
                },
            ]
        });
    }

    verDetalles(event, i) {
        event.preventDefault();
        Modal.desplegar({
            titulo: 'Caracteristicas del producto',
            contenido: this.#productos[i].detalles,
            botones: [
                {
                    id: 'btn-cerrar-infoproductos',
                    clase: 'modal-close rounded-md bg-red-500 px-4 py-2 text-white mr-2',
                    titulo: 'Cerrar',
                }
            ]
        });
    }

    editarFila(event, i) {
        event.preventDefault();
        Modal.desplegar({
            titulo: `Edición del producto`,
            contenido: this.formularioEdicion(i),
            botones: [
                {
                    id: 'btn-guardar-infoproducto',
                    clase: 'modal-close rounded-md bg-teal-700 px-4 py-2 text-white mr-2',
                    titulo: 'Guardar',
                    callBack: this.guardarEdicion
                },
                {
                    id: 'btn-cerrar-infoproductos',
                    clase: 'modal-close rounded-md bg-red-500 px-4 py-2 text-white',
                    titulo: 'Cerrar', 
                },
            ]
        });
    }

    formularioAdicion(){
        this.#formEdicion = this.#formEdicion.replace('productos-edicion', 'productos-adicion');
        const busqueda = ['$identificador', '$referencia', '$disponible', '$precio', '$resumen', '$caracteristicas', '$imagen'];
        const reemplazo = ['', '', '', '', '', '', ''];
        return this.#formEdicion.replaceArray(busqueda, reemplazo);
    }

    formularioEdicion(i){
        this.#formEdicion = this.#formEdicion.replace('productos-edicion', `productos-edicion-${i}`);
        const busqueda = ['$identificador', '$referencia', '$disponible', '$precio', '$resumen', '$caracteristicas', '$imagen'];
        const reemplazo = Object.values(this.productos[i]);
        return this.#formEdicion.replaceArray(busqueda, reemplazo);
    }

    guardarEdicion(e) {
        console.log(`Se pulso sobre ${e.target.id}`)
    }

    guardarAdicion(e) {
        console.log(`Se pulso sobre ${e.target.id}`)
    }

    eliminarFila(event, i) {
        event.preventDefault();
        Modal.desplegar({
            titulo: `Eliminar ${this.#productos[i].referencia}`,
            contenido: `Se eliminó exitosamente de la lista de productos.<br>
                        Por favor actualice la tabla de producto dando click sobre el botón.
                        <footer class="flex justify-end">
                            <button id="btnEliminarProducto"
                                class="btn btn-primary">
                                Actualizar tabla</button>
                        </footer> `,
            botones: [
                {
                    id: 'btn-cerrar-eliminar',
                    titulo: 'Cerrar',
                    callBack: this.cualquierCosa
                }
            ]
        });

        this.#productos.splice(i, 1);
        console.log(event.target.id);
        console.log(this.#productos);

        document.querySelector('#btnEliminarProducto').addEventListener(
            'click', e => this.gestionarProductos(e)
        );
    }
}